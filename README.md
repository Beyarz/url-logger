# URL Logger

## Getting started

```
git clone https://gitlab.com/Beyarz/url-logger.git
cd url-logger
bundle
```
## macOS

`docker run -v $(pwd):/app -w /app --name url-logger -p 3010:3010 --rm -it ruby:3.0.0 sh`

## Run in Docker from Ubuntu

`docker run -v $(pwd):/app -w /app --name url-logger -p 3010:3010 --rm -it ruby:3.0.0 sh ./entrypoint.sh`

### Detached

`docker run -v $(pwd):/app -w /app --name url-logger -p 3010:3010 -d --rm -it ruby:3.0.0 sh ./entrypoint.sh`
