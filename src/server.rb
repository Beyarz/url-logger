# frozen_string_literal: true

require "sinatra"
require "sinatra/reloader" if development?
require "csv"
require "base64"

FILE_NAME = "logs.csv"
File.new(FILE_NAME, "w") unless File.exist?(FILE_NAME)

set :port, 3010
set :bind, "0.0.0.0"

get "/" do
  paths = CSV.open(FILE_NAME)
  erb :index, locals: { logs: paths.read }
end

# path must be base64 encoded
get "/log/:path" do
  base64_decode_param = Base64.decode64 params["path"]
  log = "#{base64_decode_param}, #{Time.now}"

  File.open(FILE_NAME, "a") do |file|
    file.puts log
  end

  erb "Path logged!", format: :plain
end

not_found do
  erb :error_not_found
end
